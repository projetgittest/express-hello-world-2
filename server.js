const express = require('express');

const port = process.env.PORT || 3000;
const app = express();

// Test de commentaire

app.get('/', (req, res) => {
  res.send('Hello World !!! (Version ##APP_VERSION##)')
});

listen();

function listen() {
  app.listen(port);
  console.log('Express app started on port ' + port);
}
